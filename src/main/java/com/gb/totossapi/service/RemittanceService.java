package com.gb.totossapi.service;

import com.gb.totossapi.entity.Member;
import com.gb.totossapi.entity.Remittance;
import com.gb.totossapi.model.remittance.RemittanceCreateRequest;
import com.gb.totossapi.model.remittance.RemittanceItem;
import com.gb.totossapi.model.remittance.RemittanceNameChangeRequest;
import com.gb.totossapi.model.remittance.RemittanceResponse;
import com.gb.totossapi.repository.RemittanceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RemittanceService {
    private final RemittanceRepository remittanceRepository;

    public void setRemittance(Member member, RemittanceCreateRequest request) {
        Remittance addData = new Remittance();

        addData.setMember(member);
        addData.setDateTrade(LocalDate.now());
        addData.setDepositor(request.getDepositor());
        addData.setBank(request.getBank());
        addData.setAccountNumber(request.getAccountNumber());

        remittanceRepository.save(addData);
    }

    public List<RemittanceItem> getRemittances() {
        List<Remittance> originList = remittanceRepository.findAll();
        List<RemittanceItem> result = new LinkedList<>();

        for (Remittance remittance : originList) {
            RemittanceItem addItem = new RemittanceItem();

            addItem.setMemberName(remittance.getMember().getName());
            addItem.setDepositor(remittance.getDepositor());
            addItem.setBank(remittance.getBank().getName());

            result.add(addItem);
        }
        return result;
    }

    public RemittanceResponse getRemittance(long id) {
        Remittance originData = remittanceRepository.findById(id).orElseThrow();
        RemittanceResponse response = new RemittanceResponse();

        response.setMemberId(originData.getMember().getId());
        response.setMemberName(originData.getMember().getName());
        response.setDateTrade(originData.getDateTrade());
        response.setDepositor(originData.getDepositor());
        response.setBank(originData.getBank().getName());
        response.setAccountNumber(originData.getAccountNumber());
        return response;
    }

    public void putRemittanceName(long id, RemittanceNameChangeRequest request) {
        Remittance originData = remittanceRepository.findById(id).orElseThrow();

        originData.setDepositor(request.getDepositor());

        remittanceRepository.save(originData);
    }

    public void delRemittance(long id) {
        remittanceRepository.deleteById(id);
    }
}
