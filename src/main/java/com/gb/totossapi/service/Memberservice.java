package com.gb.totossapi.service;

import com.gb.totossapi.entity.Member;
import com.gb.totossapi.model.member.MemberCreateRequest;
import com.gb.totossapi.model.member.MemberItem;
import com.gb.totossapi.model.member.MemberNameChangeRequest;
import com.gb.totossapi.model.member.MemberResponse;
import com.gb.totossapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class Memberservice {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setBank(request.getBank());
        addData.setAccountNumber(request.getAccountNumber());

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for (Member member : originList) {
            MemberItem addItem = new MemberItem();
            addItem.setName(member.getName());
            addItem.setBank(member.getBank().getName());
            result.add(addItem);
        }
        return result;
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();

        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setBank(originData.getBank().getName());
        response.setAccountNumber(originData.getAccountNumber());
        return response;
    }

    public void putMemberName(long id, MemberNameChangeRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow();
        originData.setName(request.getName());

        memberRepository.save(originData);
    }
}
