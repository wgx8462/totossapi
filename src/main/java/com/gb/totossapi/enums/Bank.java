package com.gb.totossapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Bank {
    KB("국민"),
    WOORI("우리"),
    SHINHAN("신한"),
    IBK("기업");

    private final String name;
}
