package com.gb.totossapi.controller;

import com.gb.totossapi.model.member.MemberCreateRequest;
import com.gb.totossapi.model.member.MemberItem;
import com.gb.totossapi.model.member.MemberNameChangeRequest;
import com.gb.totossapi.model.member.MemberResponse;
import com.gb.totossapi.service.Memberservice;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final Memberservice memberservice;

    @PostMapping("/join")
    public String setMember(@RequestBody MemberCreateRequest request) {
        memberservice.setMember(request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberservice.getMembers();
    }

    @GetMapping("/detail/{id}")
    public MemberResponse getMember(@PathVariable long id) {
        return memberservice.getMember(id);
    }

    @PutMapping("/name/{id}")
    public String putMemberName(@PathVariable long id, @RequestBody MemberNameChangeRequest request) {
        memberservice.putMemberName(id, request);

        return "수정 완료";
    }
}
