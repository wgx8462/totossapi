package com.gb.totossapi.controller;

import com.gb.totossapi.entity.Member;
import com.gb.totossapi.model.remittance.RemittanceCreateRequest;
import com.gb.totossapi.model.remittance.RemittanceItem;
import com.gb.totossapi.model.remittance.RemittanceNameChangeRequest;
import com.gb.totossapi.model.remittance.RemittanceResponse;
import com.gb.totossapi.service.Memberservice;
import com.gb.totossapi.service.RemittanceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/remittance")
public class RemittanceController {
    private final Memberservice memberservice;
    private final RemittanceService remittanceService;

    @PostMapping("/new/member-id/{memberId}")
    public String setRemittance(@PathVariable long memberId, @RequestBody RemittanceCreateRequest request) {
        Member member = memberservice.getData(memberId);
        remittanceService.setRemittance(member, request);

        return "등록 완료";
    }

    @GetMapping("/all")
    public List<RemittanceItem> getRemittances() {
        return remittanceService.getRemittances();
    }

    @GetMapping("/detail/remittance-id/{remittanceId}")
    public RemittanceResponse getRemittance(@PathVariable long remittanceId) {
        return remittanceService.getRemittance(remittanceId);
    }

    @PutMapping("/name-change/remittance-id/{remittanceId}")
    public String putRemittanceName(@PathVariable long remittanceId, @RequestBody RemittanceNameChangeRequest request) {
        remittanceService.putRemittanceName(remittanceId, request);

        return "수정 완료";
    }

    @DeleteMapping("/remittance-id/{remittanceId}")
    public String delRemittance(@PathVariable long remittanceId) {
        remittanceService.delRemittance(remittanceId);

        return "삭제 완료";
    }
}
