package com.gb.totossapi.entity;

import com.gb.totossapi.enums.Bank;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Bank bank;

    @Column(nullable = false, length = 9, unique = true)
    private String accountNumber;
}
