package com.gb.totossapi.entity;

import com.gb.totossapi.enums.Bank;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Remittance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private LocalDate dateTrade;

    @Column(nullable = false, length = 20)
    private String depositor;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Bank bank;

    @Column(nullable = false, length = 9)
    private String accountNumber;
}
