package com.gb.totossapi.repository;

import com.gb.totossapi.entity.Remittance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RemittanceRepository extends JpaRepository<Remittance, Long> {
}
