package com.gb.totossapi.model.remittance;

import com.gb.totossapi.enums.Bank;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RemittanceCreateRequest {
    private LocalDate dateTrade;
    private String depositor;
    @Enumerated(value = EnumType.STRING)
    private Bank bank;
    private String accountNumber;
}
