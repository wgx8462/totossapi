package com.gb.totossapi.model.remittance;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RemittanceNameChangeRequest {
    private String depositor;
}
