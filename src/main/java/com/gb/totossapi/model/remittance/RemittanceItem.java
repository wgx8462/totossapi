package com.gb.totossapi.model.remittance;

import com.gb.totossapi.enums.Bank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RemittanceItem {
    private String memberName;
    private String depositor;
    private String bank;
}
