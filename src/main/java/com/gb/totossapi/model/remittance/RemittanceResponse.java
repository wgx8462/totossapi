package com.gb.totossapi.model.remittance;

import com.gb.totossapi.enums.Bank;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class RemittanceResponse {
    private Long memberId;
    private String memberName;
    private LocalDate dateTrade;
    private String depositor;
    private String bank;
    private String accountNumber;
}
