package com.gb.totossapi.model.member;

import com.gb.totossapi.enums.Bank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberResponse {
    private Long id;
    private String name;
    private String bank;
    private String accountNumber;
}
