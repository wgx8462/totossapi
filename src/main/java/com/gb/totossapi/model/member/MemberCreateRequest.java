package com.gb.totossapi.model.member;

import com.gb.totossapi.enums.Bank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreateRequest {
    private String name;
    private Bank bank;
    private String accountNumber;
}
