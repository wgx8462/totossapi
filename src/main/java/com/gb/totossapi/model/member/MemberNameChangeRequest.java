package com.gb.totossapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberNameChangeRequest {
    private String name;
}
